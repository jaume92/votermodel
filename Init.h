//
//  Init.h
//  Voter
//
//
// Jaume Muntsant (1271258)

#define SIZE 50 //Matrix Size

#define TERM "\e[37m\e[40m"   //Terminal settings
#define FI "\e[0m"
#define NETEJA "\e[2J\e[1;1H"

#define KNRM  "\x1B[0m"   //Colours
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

extern int table[SIZE][SIZE];
extern int provisional[SIZE][SIZE];

void init_s(int num);
void init_c();