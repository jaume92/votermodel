//
//  Table.c
//  Voter
//
//
// Jaume Muntsant (1271258)

#include <stdio.h>

#include "Init.h"

void print(int iter, int init){
    int i, j, white=0, black=0, red=0, blue=0, green=0, yellow=0, cyan=0, magenta=0 ;
    
    int x,y;
    int x_center=SIZE/2;
    int y_center=SIZE/2;
    int radius=SIZE/4;
    
    for (i=0; i<SIZE; i++) {
        for (j=0; j<SIZE; j++) {
            if (table[i][j]==0) {
                black++;
            }
            if (table[i][j]==1) {
                white++;
            }
            if (table[i][j]==2) {
                red++;
            }
            if (table[i][j]==3) {
                blue++;
            }
            if (table[i][j]==4) {
                green++;
            }
            if (table[i][j]==5) {
                yellow++;
            }
            if (table[i][j]==6) {
                cyan++;
            }
            if (table[i][j]==7) {
                magenta++;
            }
        }
        
    }
    
    printf(KWHT"ITERATION: %i\t  ",iter);
    printf(KWHT"WHITE: %i ",white);
    printf(KWHT"BLACK: %i ", black);
    printf(KRED"RED: %i ", red);
    printf(KBLU"BLUE: %i ", blue);
    printf(KGRN"GREEN: %i ", green);
    printf(KYEL"YELLOW: %i ", yellow);
    printf(KCYN"CYAN: %i ", cyan);
    printf(KMAG"MAGENTA: %i\n", magenta);
    
    for (i=0; i<SIZE; i++) {
        for (j=0; j<SIZE; j++) {
            
            x=j-x_center;
            y=i-y_center;
            
            if (init==2 && x*x+y*y<=(radius+1)*(radius+1) && x*x+y*y>=(radius)*(radius)){
                printf(KRED" \u2588");
                continue;
            }
            
            printf("");
            if (table[i][j]==0) {
                printf("  ");
            }
            if (table[i][j]==1) {
                printf(KWHT" \u2588");
            }
            if (table[i][j]==2) {
                printf(KRED" \u2588");
            }
            if (table[i][j]==3) {
                printf(KBLU" \u2588");
            }
            if (table[i][j]==4) {
                printf(KGRN" \u2588");
            }
            if (table[i][j]==5) {
                printf(KYEL" \u2588");
            }
            if (table[i][j]==6) {
                printf(KCYN" \u2588");
            }
            if (table[i][j]==7) {
                printf(KMAG" \u2588");
            }
        }
        printf("\n");
    }
}

void tabletoprov(){
    int i, j;
    
    for (i=0; i<SIZE; i++) {
        for (j=0; j<SIZE; j++) {
            provisional[i][j]=table[i][j];
        }
    }
}
