# Voter Model #

Es tracta d'un exercici on es visualitza el model del votant, fet per l'assignatura del Recerca i Innovació del màster de modelització (2016).

El model del votant es basa en la següent idea: comencem amb una població (representada per una xarxa o graf) on cada individu té una opinió que pot ser compartida amb altres individus si es troben connectats amb ell en el graf. En aquest cas considerarem que cada individu té 8 veïns amb qui intercanviar opinions.
[Wikipedia](https://en.wikipedia.org/wiki/Voter_model)

El programa presenta tres variants del model del votant:

*1.* **Standard Model:** Cada individu pregunta a un veí aleatòriament i pren la seva idea.
![Captura de pantalla 2016-11-12 a les 12.03.47.png](https://bitbucket.org/repo/A5XAjy/images/1944538681-Captura%20de%20pantalla%202016-11-12%20a%20les%2012.03.47.png)
*2.* **Double Polling Model:** Aquí cada individu pregunta a dos veïns les seves opinions. Si són iguals l'individu assumeix aquesta idea, sinó és així manté la opinió original.
![Captura de pantalla 2016-11-12 a les 12.05.25.png](https://bitbucket.org/repo/A5XAjy/images/1363048013-Captura%20de%20pantalla%202016-11-12%20a%20les%2012.05.25.png)
*3.* **Friend of Friend Model:** Aquí triem un veí aleatoriament i llencem una moneda, amb un 50% de probabilitat l'individu canviarà la seva opinió. Sinó s'agafa aleatòriament un veí del primer veí seleccionat i es repeteix el mateix procediment.
![Captura de pantalla 2016-11-12 a les 12.10.22.png](https://bitbucket.org/repo/A5XAjy/images/2001066954-Captura%20de%20pantalla%202016-11-12%20a%20les%2012.10.22.png)

### Per compilar ###

**1.** Escriure ``make``al terminal.

**2.** Executar amb ``./Voter init model step``.

* *init*: Amb *1* creem una xarxa de individus. Amb *2* s'estudia la interacció entre spins suposant que tenim inicialment un sistema d'spins configurats en un cercle.

* *model*: Tipus de model (*1,2,3*) seguint el que hem explicat amunt.

* *step*: Pas de temps del model

### Exemple ###

Exemple: ``./Voter 1 1 1`` o ``./Voter 1 2 10``