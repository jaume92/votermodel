//
//  Init.c
//  Voter
//
//
// Jaume Muntsant (1271258)

#include <stdio.h>
#include <stdlib.h>

#include "Init.h"


void init_s(int num){   //Random Initialization
    int i, j, ran;
    
    for (i=0; i<SIZE; i++) {
        for (j=0; j<SIZE; j++) {
            ran=rand()%num;
            table[i][j]=ran;
        }
    }
}

void init_c(int size){   //Circle Initialization
    
    int i, j, x, y;
    int x_center=SIZE/2;
    int y_center=SIZE/2;
    int radius=SIZE/4;
    
    for (i=0; i<SIZE; i++) {
        for (j=0; j<SIZE; j++) {
            x=j-x_center;
            y=i-y_center;
            
            if (x*x+y*y<=radius*radius)
                table[i][j]=1;
            else
                table[i][j]=0;
        }
    }
}