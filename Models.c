//
//  Models.c
//  Voter
//
//
// Jaume Muntsant (1271258)

#include <stdio.h>
#include <stdlib.h>

#include "Init.h"

int mod(int a, int b) //Modulo operation
{
    int r = a % b;
    return r < 0 ? r + b : r;
}

void voter1(int r, int c){ //Standard Model
    int ran;
    
    ran=rand()%8;
    
    switch (ran) {
        case 0:
            table[r][c]=provisional[mod(r-1, SIZE)][mod(c-1, SIZE)];
            break;
        case 1:
            table[r][c]=provisional[mod(r-1, SIZE)][mod(c, SIZE)];
            break;
        case 2:
            table[r][c]=provisional[mod(r-1, SIZE)][mod(c+1, SIZE)];
            break;
        case 3:
            table[r][c]=provisional[mod(r, SIZE)][mod(c-1, SIZE)];
            break;
        case 4:
            table[r][c]=provisional[mod(r, SIZE)][mod(c+1, SIZE)];
            break;
        case 5:
            table[r][c]=provisional[mod(r+1, SIZE)][mod(c-1, SIZE)];
            break;
        case 6:
            table[r][c]=provisional[mod(r+1, SIZE)][mod(c, SIZE)];
            break;
        case 7:
            table[r][c]=provisional[mod(r+1, SIZE)][mod(c+1, SIZE)];
            break;
        default:
            break;
    }
}

void voter2(int r, int c){ //Double Polling Model
    int ran1=-1, ran2=-1;
    int op1, op2;
    
    ran1=rand()%8;
    
    do {
        ran2=rand()%8;
    } while (ran1==ran2);
    
    switch (ran1) {
        case 0:
            op1=provisional[mod(r-1, SIZE)][mod(c-1, SIZE)];
            break;
        case 1:
            op1=provisional[mod(r-1, SIZE)][mod(c, SIZE)];
            break;
        case 2:
            op1=provisional[mod(r-1, SIZE)][mod(c+1, SIZE)];
            break;
        case 3:
            op1=provisional[mod(r, SIZE)][mod(c-1, SIZE)];
            break;
        case 4:
            op1=provisional[mod(r, SIZE)][mod(c+1, SIZE)];
            break;
        case 5:
            op1=provisional[mod(r+1, SIZE)][mod(c-1, SIZE)];
            break;
        case 6:
            op1=provisional[mod(r+1, SIZE)][mod(c, SIZE)];
            break;
        case 7:
            op1=provisional[mod(r+1, SIZE)][mod(c+1, SIZE)];
            break;
        default:
            break;
    }
    
    switch (ran2) {
        case 0:
            op2=provisional[mod(r-1, SIZE)][mod(c-1, SIZE)];
            break;
        case 1:
            op2=provisional[mod(r-1, SIZE)][mod(c, SIZE)];
            break;
        case 2:
            op2=provisional[mod(r-1, SIZE)][mod(c+1, SIZE)];
            break;
        case 3:
            op2=provisional[mod(r, SIZE)][mod(c-1, SIZE)];
            break;
        case 4:
            op2=provisional[mod(r, SIZE)][mod(c+1, SIZE)];
            break;
        case 5:
            op2=provisional[mod(r+1, SIZE)][mod(c-1, SIZE)];
            break;
        case 6:
            op2=provisional[mod(r+1, SIZE)][mod(c, SIZE)];
            break;
        case 7:
            op2=provisional[mod(r+1, SIZE)][mod(c+1, SIZE)];
            break;
        default:
            break;
    }
    
    if (op1==op2)
        table[r][c]=op1;
}


void voter3(int r1, int c1, int r, int c){   //Friend to Friend Model
    int ran1, rnou=0, cnou=0;
    
    ran1=rand()%8;
    
    switch (ran1) {
        case 0:
            rnou=mod(r-1, SIZE);
            cnou=mod(c-1, SIZE);
            break;
        case 1:
            rnou=mod(r-1, SIZE);
            cnou=mod(c, SIZE);
            break;
        case 2:
            rnou=mod(r-1, SIZE);
            cnou=mod(c+1, SIZE);
            break;
        case 3:
            rnou=mod(r, SIZE);
            cnou=mod(c-1, SIZE);
            break;
        case 4:
            rnou=mod(r, SIZE);
            cnou=mod(c+1, SIZE);
            break;
        case 5:
            rnou=mod(r+1, SIZE);
            cnou=mod(c-1, SIZE);
            break;
        case 6:
            rnou=mod(r+1, SIZE);
            cnou=mod(c, SIZE);
            break;
        case 7:
            rnou=mod(r+1, SIZE);
            cnou=mod(c+1, SIZE);
            break;
        default:
            break;
    }
    
    if (rand()%2==0)
        table[r1][c1]=provisional[rnou][cnou];
    else
        voter3(r1, c1, rnou, cnou);
}