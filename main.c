//
//  main.c
//  Voter
//
//
// Jaume Muntsant (1271258)

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "Init.h"
#include "Models.h"
#include "Table.h"

int table[SIZE][SIZE];
int provisional[SIZE][SIZE];


int main( int argc, char *argv[]) {
    int i, j, op, iter=0;
    
    int init,model,step;
    
    if (argc<4
        || sscanf(argv[1],"%d",&init)!=1
        || sscanf(argv[2], "%d",&model)!=1
        || sscanf(argv[3], "%d",&step)!=1) {
        fprintf(stderr,"%s: init model step\n",argv[0]);
        return 1;
    }
    
    printf(TERM);
    printf(NETEJA);
    srand((unsigned int)time(NULL));
    
    switch (init) {
        case 1:
            while (op<1 || op>8) {
                printf("Number of opinions (1-8)?\n");
                if (fscanf(stdin,"%d",&op)!=1) {
                    fprintf(stderr,"%s: error reading number\n", argv[0]);
                    return 1;
                }
                
            }
            init_s(op);
            break;
        case 2:
            init_c();
            break;
        default:
            fprintf(stderr, "Error: Init Option\n");
            return 1;
            break;
    }
    
    system("clear");
    print(iter,init);
    sleep(1);
    
    while(1){
        tabletoprov();
        
        for (i=0; i<SIZE; i++) {
            for (j=0; j<SIZE; j++) {
                switch (model) {
                    case 1:
                        voter1(i, j);
                        break;
                    case 2:
                        voter2(i, j);
                        break;
                    case 3:
                        voter3(i, j, i, j);
                        break;
                    default:
                        fprintf(stderr, "Error: Model Option\n");
                        break;
                }
            }
        }
        if (iter%step==0) {
            system("clear");
            print(iter,init);
            sleep(1);
        }
        iter++;
    }
    return 0;
}
